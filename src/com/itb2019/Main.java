package com.itb2019;

import java.util.Arrays;

public class Main {

    static final int DIES_SETMANA = 7;
    static final int DIES_LABORABLES = 5;
    static Textos textos;


    public static void main(String[] args) {
        System.out.println("args = " + Arrays.deepToString(args));
        System.out.println("-----------------------------------");

        textos = new Textos();
        if(args.length == 0){
            System.out.println("args = " + textos.getNo_controlat());
            System.out.println("- Sense arguments");
            System.exit(0);
        }

        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            switch (args[i]) {
                case Constants.IDIOMA_CATALA:
                    System.out.println(textos.getDiesLaborals1CA() + DIES_LABORABLES + textos.getDiesLaborals2CA());
                    break;
                case Constants.IDIOMA_ESPANYOL:
                    System.out.println(textos.getDiesLaborals1CA() + DIES_LABORABLES + textos.getDiesLaborals2CA());
                    break;
                case Constants.IDIOMA_INGLES:
                    System.out.println(textos.getDiesLaborals1CA() + DIES_LABORABLES + textos.getDiesLaborals2CA());
                    break;
                default:
                    System.out.println("args = " + textos.getNo_controlat());
                    System.out.println("Idioma no reconegut");
            }
        }

        System.out.println("\n--------- UNA ALTRA OPCIÓ: ---------");

        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            System.out.println(textos.fraseDiesLaborals(args[i], DIES_LABORABLES));
        }
    }

}
