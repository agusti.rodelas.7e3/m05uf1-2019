package com.itb2019;

public class Textos {

    public Textos() {
    }

    private String diesLaborals1CA = "La setmana te ";
    private String diesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String diesLaborals1EN = "The week has ";
    private String diesLaborals2EN = "working days.";

    private String no_controlat = "[!] Idioma no controlat";

    private String no_arguments = "No s'han indicat arguments";


    public String getDiesLaborals1CA() {
        return diesLaborals1CA;
    }

    public String getDiesLaborals2CA() {
        return diesLaborals2CA;
    }

    public String getDiesLaborals1ES() {
        return diesLaborals1ES;
    }

    public String getDiesLaborals2ES() {
        return diesLaborals2ES;
    }

    public String getDiesLaborals1EN() {
        return diesLaborals1EN;
    }

    public String getDiesLaborals2EN() {
        return diesLaborals2EN;
    }

    public String getNo_controlat() {
        return no_controlat;
    }

    public String getNo_arguments() { return no_arguments;}

    public String fraseDiesLaborals(String idioma, int diesLaborals) {
        String frase = no_controlat;

        switch (idioma) {
            case Constants.IDIOMA_ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + Constants.ESPAI + diesLaborals2ES;
                break;
            case Constants.IDIOMA_CATALA:
                frase = diesLaborals1ES + diesLaborals + Constants.ESPAI + diesLaborals2ES;
                break;
            case Constants.IDIOMA_INGLES:
                frase = diesLaborals1ES + diesLaborals + Constants.ESPAI + diesLaborals2ES;
                break;
            default:
                break;
        }
        return frase;

    }

}
